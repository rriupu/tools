## Get conservation scores

The purpose of this script is to take one or more BED files and build a plot for each one with the nucleotide conservation scores of a window centered at its coordinates.

**Important! The computation of nucleotide conservation can use a lot of RAM and time when the BED files have a lot of rows and the window size is considerably big. For reference, the computation of conservation scores for a BED file with ~30 million rows and a window of 1000 bp took about 500GB of RAM and 24 hours of computations.**

### Software requirements:

- bedtools
- bwtool
- R base
- ggplot2 R package
- tidyverse R package

### Arguments:

All the arguments below are mandatory:

```
-i INPUT BED: input BED file with the coordinates of interest.
-c CONSERVATION FILE(S): comma-separated paths to the bigWig files (.bw extension) with nucleotide conservation scores. Example: -c file1.bw,file2.bw,file3.bw
-w WINDOW: number of nucleotides to the right and left of the BED coordinates to extract the conservation scores.
-s CHROMS SIZES: path to the chromosome sizes file.
-o OUTPUT DIR: directory where the output will be saved.
```

### Example:

In the example below, the script will compute for file1.bed and file2.bed the conservation scores for 1000 bp windows centered at their coordinates and make a plot for each one of the BED files. The script also randomly shuffles both BED files to plot a random track.

```
get_conservation.sh -i file1.bed,file2.bed -c file1.bw,file2.bw,file3.bw -w 1000 -s hg38.chroms.sizes -o output_dir/
```
