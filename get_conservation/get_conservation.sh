#!/bin/bash

DIR="$( cd "$( dirname "${0}" )" >/dev/null 2>&1 && pwd )"

usage() {
  echo "Usage: $0 -i INPUT BED -c INPUT CONSERVATION FILE(S) -w WINDOW -s CHROMS SIZES FILE -o OUTPUT DIR" 1>&2 
}
exit_abnormal() {
  usage
  exit 1
}

printf "Parsing arguments\n"

while getopts ":i:c:w:s:o:h" options; do
  case "${options}" in
    i)
      bed_files=${OPTARG}
      bed_files=(${bed_files//","/" "})
      ;;
    c)
      conservation_files=${OPTARG}
      ;;
    w)
      number_nts=${OPTARG}
      ;;
    s)
      chroms_sizes=${OPTARG}
      ;;
    o)
      output_dir=${OPTARG}
      ;;
    h)
      echo "Usage: $0 -i INPUT BED -c INPUT CONSERVATION FILE(S) -w WINDOW -s CHROMS SIZES FILE -o OUTPUT DIR"
      echo ""
      echo "Get and plot conservation scores in a defined window centered at coordinates from a BED file."
      echo ""
      echo "Arguments:"
      echo ""
      echo "    -i INPUT BED: input BED file with the coordinates of interest."
      echo "    -c CONSERVATION FILE(S): comma-separated paths to the bigWig files (.bw extension) with nucleotide conservation scores. Example: -c file1.bw,file2.bw,file3.bw"
      echo "    -w WINDOW: number of nucleotides to the right and left of the BED coordinates to extract the conservation scores."
      echo "    -s CHROMS SIZES: path to the chromosome sizes file."
      echo "    -o OUTPUT DIR: directory where the output will be saved."
      echo ""
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      exit_abnormal
      exit 1
      ;;
  esac
done

if [ ! -d $output_dir ]
then
  mkdir -p $output_dir
fi

for bed in ${bed_files[@]}
do
  
  printf "Computing conservation scores for: $bed\n"

  bwtool agg -h ${number_nts}:${number_nts} $bed $conservation_files $output_dir/$(basename -s ".bed" $bed)_conservation.tsv

  bedtools shuffle -chrom -i $bed -g $chroms_sizes | bedtools sort -i stdin > $output_dir/$(basename -s ".bed" $bed)_random.bed

  bwtool agg -h ${number_nts}:${number_nts} $output_dir/$(basename -s ".bed" $bed)_random.bed $conservation_files $output_dir/$(basename -s ".bed" $bed)_conservation_random.tsv

  R --silent --vanilla --slave -f $DIR/plot_conservation.R --args $output_dir/$(basename -s ".bed" $bed)_conservation.tsv $conservation_files $output_dir/$(basename -s ".bed" $bed)_conservation.pdf

done
